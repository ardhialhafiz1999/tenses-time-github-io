(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [
		{name:"It_s Time For Tenses_atlas_1", frames: [[0,0,1476,907],[1638,1521,332,94],[1093,1619,531,35],[0,909,2044,417],[218,1653,195,69],[0,1639,216,77],[1905,0,140,139],[502,1535,155,155],[264,1437,342,96],[0,1328,380,107],[608,1437,342,96],[382,1328,380,107],[1905,141,140,139],[659,1535,155,155],[1692,626,306,86],[1296,1521,340,96],[264,1535,236,116],[0,1437,262,129],[1478,151,382,134],[1478,0,425,149],[816,1619,275,74],[1725,1441,278,77],[0,1568,250,69],[1638,1617,278,77],[952,1521,342,96],[764,1328,380,107],[1146,1328,191,191],[1478,287,212,212],[1478,813,412,86],[1478,715,458,96],[1725,1328,305,111],[1692,501,339,123],[1339,1328,191,191],[1692,287,212,212],[1532,1328,191,191],[1478,501,212,212]]}
];


(lib.AnMovieClip = function(){
	this.actionFrames = [];
	this.gotoAndPlay = function(positionOrLabel){
		cjs.MovieClip.prototype.gotoAndPlay.call(this,positionOrLabel);
	}
	this.play = function(){
		cjs.MovieClip.prototype.play.call(this);
	}
	this.gotoAndStop = function(positionOrLabel){
		cjs.MovieClip.prototype.gotoAndStop.call(this,positionOrLabel);
	}
	this.stop = function(){
		cjs.MovieClip.prototype.stop.call(this);
	}
}).prototype = p = new cjs.MovieClip();
// symbols:



(lib.CachedBmp_2162 = function() {
	this.initialize(img.CachedBmp_2162);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2560,1440);


(lib.CachedBmp_2161 = function() {
	this.initialize(img.CachedBmp_2161);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2560,1440);


(lib.CachedBmp_2160 = function() {
	this.initialize(img.CachedBmp_2160);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2560,1440);


(lib.CachedBmp_2159 = function() {
	this.initialize(img.CachedBmp_2159);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2560,1440);


(lib.CachedBmp_2158 = function() {
	this.initialize(img.CachedBmp_2158);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2560,1440);


(lib.CachedBmp_2157 = function() {
	this.initialize(img.CachedBmp_2157);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2560,1440);


(lib.CachedBmp_2156 = function() {
	this.initialize(img.CachedBmp_2156);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2560,1440);


(lib.CachedBmp_2155 = function() {
	this.initialize(img.CachedBmp_2155);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2560,1440);


(lib.CachedBmp_2154 = function() {
	this.initialize(img.CachedBmp_2154);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2560,1440);


(lib.CachedBmp_2153 = function() {
	this.initialize(img.CachedBmp_2153);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2560,1440);


(lib.CachedBmp_2152 = function() {
	this.initialize(img.CachedBmp_2152);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2560,1440);


(lib.CachedBmp_2151 = function() {
	this.initialize(img.CachedBmp_2151);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2560,1440);


(lib.CachedBmp_2352 = function() {
	this.initialize(ss["It_s Time For Tenses_atlas_1"]);
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_2351 = function() {
	this.initialize(ss["It_s Time For Tenses_atlas_1"]);
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_2350 = function() {
	this.initialize(ss["It_s Time For Tenses_atlas_1"]);
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_2349 = function() {
	this.initialize(img.CachedBmp_2349);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2258,865);


(lib.CachedBmp_2146 = function() {
	this.initialize(img.CachedBmp_2146);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2560,1440);


(lib.CachedBmp_2145 = function() {
	this.initialize(ss["It_s Time For Tenses_atlas_1"]);
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_2144 = function() {
	this.initialize(img.CachedBmp_2144);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2560,1440);


(lib.CachedBmp_2143 = function() {
	this.initialize(img.CachedBmp_2143);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2560,1440);


(lib.CachedBmp_2348 = function() {
	this.initialize(ss["It_s Time For Tenses_atlas_1"]);
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_2347 = function() {
	this.initialize(ss["It_s Time For Tenses_atlas_1"]);
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_2346 = function() {
	this.initialize(ss["It_s Time For Tenses_atlas_1"]);
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_2345 = function() {
	this.initialize(ss["It_s Time For Tenses_atlas_1"]);
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_2138 = function() {
	this.initialize(ss["It_s Time For Tenses_atlas_1"]);
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_2137 = function() {
	this.initialize(ss["It_s Time For Tenses_atlas_1"]);
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_2136 = function() {
	this.initialize(ss["It_s Time For Tenses_atlas_1"]);
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_2135 = function() {
	this.initialize(ss["It_s Time For Tenses_atlas_1"]);
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_2344 = function() {
	this.initialize(ss["It_s Time For Tenses_atlas_1"]);
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_2343 = function() {
	this.initialize(ss["It_s Time For Tenses_atlas_1"]);
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_2132 = function() {
	this.initialize(ss["It_s Time For Tenses_atlas_1"]);
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_2131 = function() {
	this.initialize(ss["It_s Time For Tenses_atlas_1"]);
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_2342 = function() {
	this.initialize(ss["It_s Time For Tenses_atlas_1"]);
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_2341 = function() {
	this.initialize(ss["It_s Time For Tenses_atlas_1"]);
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_2340 = function() {
	this.initialize(ss["It_s Time For Tenses_atlas_1"]);
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_2339 = function() {
	this.initialize(ss["It_s Time For Tenses_atlas_1"]);
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_2338 = function() {
	this.initialize(ss["It_s Time For Tenses_atlas_1"]);
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_2337 = function() {
	this.initialize(ss["It_s Time For Tenses_atlas_1"]);
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_2336 = function() {
	this.initialize(ss["It_s Time For Tenses_atlas_1"]);
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_2335 = function() {
	this.initialize(ss["It_s Time For Tenses_atlas_1"]);
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_2122 = function() {
	this.initialize(ss["It_s Time For Tenses_atlas_1"]);
	this.gotoAndStop(24);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_2121 = function() {
	this.initialize(ss["It_s Time For Tenses_atlas_1"]);
	this.gotoAndStop(25);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_2120 = function() {
	this.initialize(ss["It_s Time For Tenses_atlas_1"]);
	this.gotoAndStop(26);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_2119 = function() {
	this.initialize(ss["It_s Time For Tenses_atlas_1"]);
	this.gotoAndStop(27);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_2334 = function() {
	this.initialize(ss["It_s Time For Tenses_atlas_1"]);
	this.gotoAndStop(28);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_2333 = function() {
	this.initialize(ss["It_s Time For Tenses_atlas_1"]);
	this.gotoAndStop(29);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_2332 = function() {
	this.initialize(ss["It_s Time For Tenses_atlas_1"]);
	this.gotoAndStop(30);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_2331 = function() {
	this.initialize(ss["It_s Time For Tenses_atlas_1"]);
	this.gotoAndStop(31);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_2114 = function() {
	this.initialize(ss["It_s Time For Tenses_atlas_1"]);
	this.gotoAndStop(32);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_2113 = function() {
	this.initialize(ss["It_s Time For Tenses_atlas_1"]);
	this.gotoAndStop(33);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_2112 = function() {
	this.initialize(ss["It_s Time For Tenses_atlas_1"]);
	this.gotoAndStop(34);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_2111 = function() {
	this.initialize(ss["It_s Time For Tenses_atlas_1"]);
	this.gotoAndStop(35);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_2330 = function() {
	this.initialize(img.CachedBmp_2330);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2560,1440);


(lib.CachedBmp_2329 = function() {
	this.initialize(img.CachedBmp_2329);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2560,1440);


(lib.CachedBmp_2328 = function() {
	this.initialize(img.CachedBmp_2328);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2560,1440);


(lib.CachedBmp_2327 = function() {
	this.initialize(img.CachedBmp_2327);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2560,1440);


(lib.CachedBmp_2326 = function() {
	this.initialize(img.CachedBmp_2326);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2560,1440);


(lib.CachedBmp_2325 = function() {
	this.initialize(img.CachedBmp_2325);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2560,1440);


(lib.CachedBmp_2324 = function() {
	this.initialize(img.CachedBmp_2324);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2067,718);


(lib.CachedBmp_2323 = function() {
	this.initialize(img.CachedBmp_2323);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2067,718);


(lib.CachedBmp_2322 = function() {
	this.initialize(img.CachedBmp_2322);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2067,718);


(lib.CachedBmp_2321 = function() {
	this.initialize(img.CachedBmp_2321);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2067,718);


(lib.CachedBmp_2320 = function() {
	this.initialize(img.CachedBmp_2320);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2067,718);


(lib.CachedBmp_2319 = function() {
	this.initialize(img.CachedBmp_2319);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2066,718);


(lib.CachedBmp_2318 = function() {
	this.initialize(img.CachedBmp_2318);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2066,718);


(lib.CachedBmp_2317 = function() {
	this.initialize(img.CachedBmp_2317);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2066,718);


(lib.CachedBmp_2316 = function() {
	this.initialize(img.CachedBmp_2316);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2066,718);


(lib.CachedBmp_2315 = function() {
	this.initialize(img.CachedBmp_2315);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2066,718);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop, this.reversed));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.mcSound = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,0,0);


(lib.grTense12 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.CachedBmp_2162();
	this.instance.setTransform(-640,-360,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-640,-360,1280,720);


(lib.grTense11 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.CachedBmp_2161();
	this.instance.setTransform(-640,-360,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-640,-360,1280,720);


(lib.grTense10 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.CachedBmp_2160();
	this.instance.setTransform(-640,-360,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-640,-360,1280,720);


(lib.grTense9 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.CachedBmp_2159();
	this.instance.setTransform(-640,-360,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-640,-360,1280,720);


(lib.grTense8 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.CachedBmp_2158();
	this.instance.setTransform(-640,-360,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-640,-360,1280,720);


(lib.grTense7 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.CachedBmp_2157();
	this.instance.setTransform(-640,-360,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-640,-360,1280,720);


(lib.grTense6 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.CachedBmp_2156();
	this.instance.setTransform(-640,-360,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-640,-360,1280,720);


(lib.grTense5 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.CachedBmp_2155();
	this.instance.setTransform(-640,-360,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-640,-360,1280,720);


(lib.grTense4 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.CachedBmp_2154();
	this.instance.setTransform(-640,-360,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-640,-360,1280,720);


(lib.grTense3 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.CachedBmp_2153();
	this.instance.setTransform(-640,-360,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-640,-360,1280,720);


(lib.grTense2 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.CachedBmp_2152();
	this.instance.setTransform(-640,-360,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-640,-360,1280,720);


(lib.grTense1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.CachedBmp_2151();
	this.instance.setTransform(-640,-360,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-640,-360,1280,720);


(lib.grIntro = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.CachedBmp_2352();
	this.instance.setTransform(-368.95,-226.8,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-368.9,-226.8,738,453.5);


(lib.grHomelogo = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.CachedBmp_2351();
	this.instance.setTransform(-83,-23.4,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-83,-23.4,166,47);


(lib.grHomeinfo = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.CachedBmp_2350();
	this.instance.setTransform(-132.7,-8.65,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-132.7,-8.6,265.5,17.5);


(lib.grHomebg = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.CachedBmp_2146();
	this.instance.setTransform(-640,-360,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-640,-360,1280,720);


(lib.grGuide = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.CachedBmp_2145();
	this.instance.setTransform(-510.95,-104.1,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-510.9,-104.1,1022,208.5);


(lib.grGloss2 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.CachedBmp_2144();
	this.instance.setTransform(-640,-360,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-640,-360,1280,720);


(lib.grGloss1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.CachedBmp_2143();
	this.instance.setTransform(-640,-360,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-640,-360,1280,720);


(lib.Group_14 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#28282D").s().p("AqtAhQkcgNAAgUQAAgTEcgNQEdgOGQgBQGRABEdAOQEcANAAATQAAAUkcANQkbAPmTAAQmRAAkcgPg");
	this.shape.setTransform(96.975,4.75);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.Group_14, new cjs.Rectangle(0,0,194,9.5), null);


(lib.Group_13 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#28282D").s().p("AsbAhQlKgNAAgUQAAgTFKgNQFLgOHQgBQHRABFLAOQFKANAAATQAAAUlKANQlJAPnTAAQnSAAlJgPg");
	this.shape.setTransform(112.575,4.75);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.Group_13, new cjs.Rectangle(0,0,225.2,9.5), null);


(lib.Group_12 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#28282D").s().p("Ar8AhQk8gNAAgUQAAgTE8gNQE+gOG+gBQG/ABE+AOQE8ANAAATQAAAUk8ANQk8APnBAAQnAAAk8gPg");
	this.shape.setTransform(108.125,4.75);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.Group_12, new cjs.Rectangle(0,0,216.3,9.5), null);


(lib.Group_2 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#2C2C3A").s().p("AlcC1QhLAAg1g1Qg1g1AAhLQAAhLA1g0QA1g1BLgBIK5AAQBLABA1A1QA1A0AABLQAABLg1A1Qg1A1hLAAg");
	this.shape.setTransform(53.025,18.15);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.Group_2, new cjs.Rectangle(0,0,106.1,36.3), null);


(lib.Group = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#2C2C3A").s().p("An2C1QhMAAg1g1Qg1g1AAhLQAAhLA1g0QA1g1BMgBIPuAAQBLABA1A1QA1A0AABLQAABLg1A1Qg1A1hLAAg");
	this.shape.setTransform(68.475,18.15);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.Group, new cjs.Rectangle(0,0,137,36.3), null);


(lib.Path_0 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Aj1D2QhlhmgBiQQABiPBlhmQBmhmCPAAQCQAABmBmQBmBmgBCPQABCQhmBmQhmBmiQAAQiPAAhmhmg");
	this.shape.setTransform(34.75,34.775);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.Path_0, new cjs.Rectangle(0,0,69.5,69.6), null);


(lib.Path = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Aj2D4QhnhnAAiRQAAiQBnhmQBmhnCQAAQCRAABnBnQBmBmAACQQAACRhmBnQhnBmiRAAQiQAAhmhmg");
	this.shape.setTransform(35,35);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.Path, new cjs.Rectangle(0,0,70,70), null);


(lib.btnPresent = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.CachedBmp_2137();
	this.instance.setTransform(-95,-26.7,0.5,0.5);

	this.instance_1 = new lib.CachedBmp_2138();
	this.instance_1.setTransform(-85.55,-24.05,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95,-26.7,190,53.5);


(lib.btnPast = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.CachedBmp_2135();
	this.instance.setTransform(-95,-26.7,0.5,0.5);

	this.instance_1 = new lib.CachedBmp_2136();
	this.instance_1.setTransform(-85.55,-24.1,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95,-26.7,190,53.5);


(lib.btnLanjut = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.CachedBmp_2131();
	this.instance.setTransform(-85,-23.85,0.5,0.5);

	this.instance_1 = new lib.CachedBmp_2132();
	this.instance_1.setTransform(-76.45,-21.55,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-85,-23.8,170,48);


(lib.btnIntro = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.CachedBmp_2341();
	this.instance.setTransform(-65.45,-32.25,0.5,0.5);

	this.instance_1 = new lib.CachedBmp_2342();
	this.instance_1.setTransform(-58.9,-29.05,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-65.4,-32.2,131,64.5);


(lib.btnHome3 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("ABFERQgGABgGgFQgEgFAAgGIAAiBQAAgLgIgIQgIgIgLAAIgzAAQgLAAgHAIQgJAIAAALIAACBQAAAGgEAFQgEAFgIgBIhXAAQgZAAgQgRQgSgRAAgYIAAikIgKAAQgUgBgOgPQgPgOAAgVQAAgUAPgPIAAAAIDgjfQAOgOAUAAQAVAAAPAOIDfDfQAOAPAAAUQAAAVgOAOQgPAQgUAAIgJAAIAACkQAAAYgRARQgSARgYAAg");
	this.shape.setTransform(0,-2.85);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF9F48").s().p("Al8F9QieieAAjfQAAjeCeieQCeieDeAAQDfAACeCeQCeCeAADeQAADfieCeQieCejfAAQjeAAieieg");
	this.shape_1.setTransform(0.025,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1,p:{scaleX:1,scaleY:1,x:0.025,y:0}},{t:this.shape,p:{scaleX:1,scaleY:1,x:0,y:-2.85}}]}).to({state:[{t:this.shape_1,p:{scaleX:0.9,scaleY:0.9,x:0.0363,y:0.0067}},{t:this.shape,p:{scaleX:0.9,scaleY:0.9,x:0.0138,y:-2.5583}}]},2).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-53.8,-53.9,107.69999999999999,107.8);


(lib.btnHome2 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AApCjQgEAAgDgEQgCgCAAgEIAAhNQAAgGgFgFQgFgFgGAAIgeAAQgHAAgFAFQgEAFAAAGIAABNQAAAEgDACQgCAEgEAAIg0AAQgPAAgLgLQgKgKABgOIAAhiIgGAAIgBgBQgMAAgIgIQgIgJgBgMQAAgMAJgJICFiEQAJgJALABQAMgBAJAJICFCEQAIAJAAAMQAAAMgIAJQgKAJgLAAIgFAAIAABiQAAAOgLAKQgKALgOAAg");
	this.shape.setTransform(-0.0054,-2.1707,1.3123,1.3123);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#2C2C3A").s().p("AjhDiQheheAAiEQAAiEBehdQBdheCEAAQCEAABeBeQBeBdAACEQAACFheBdQheBeiEAAQiEAAhdheg");
	this.shape_1.setTransform(-0.0054,0.0274,1.3123,1.3123);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1,p:{scaleX:1.3123,scaleY:1.3123,x:-0.0054,y:0.0274}},{t:this.shape,p:{scaleX:1.3123,scaleY:1.3123,x:-0.0054,y:-2.1707}}]}).to({state:[{t:this.shape_1,p:{scaleX:1.1998,scaleY:1.1998,x:0.0431,y:0.0731}},{t:this.shape,p:{scaleX:1.1998,scaleY:1.1998,x:0.0431,y:-1.9366}}]},2).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42,-42,84,84.1);


(lib.btnHome1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("ABFERQgGABgGgFQgEgFAAgGIAAiBQAAgLgIgIQgIgIgLAAIgzAAQgLAAgHAIQgJAIAAALIAACBQAAAGgEAFQgEAFgIgBIhXAAQgZAAgQgRQgSgRAAgYIAAikIgKAAQgUgBgOgPQgPgOAAgVQAAgUAPgPIAAAAIDgjfQAOgOAUAAQAVAAAPAOIDfDfQAOAPAAAUQAAAVgOAOQgPAQgUAAIgJAAIAACkQAAAYgRARQgSARgYAAg");
	this.shape.setTransform(0,-2.85);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF9F48").s().p("Al8F9QieieAAjfQAAjeCeieQCeieDeAAQDfAACeCeQCeCeAADeQAADfieCeQieCejfAAQjeAAieieg");
	this.shape_1.setTransform(0.025,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1,p:{scaleX:1,scaleY:1,x:0.025,y:0}},{t:this.shape,p:{scaleX:1,scaleY:1,x:0,y:-2.85}}]}).to({state:[{t:this.shape_1,p:{scaleX:0.8999,scaleY:0.8999,x:0.0347,y:0.0019}},{t:this.shape,p:{scaleX:0.8999,scaleY:0.8999,x:0.0122,y:-2.5629}}]},2).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-53.8,-53.9,107.69999999999999,107.8);


(lib.btnGuide = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.CachedBmp_2339();
	this.instance.setTransform(-106.1,-37.3,0.5,0.5);

	this.instance_1 = new lib.CachedBmp_2340();
	this.instance_1.setTransform(-95.45,-33.55,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-106.1,-37.3,212.5,74.5);


(lib.btnFuture = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.CachedBmp_2121();
	this.instance.setTransform(-95,-26.7,0.5,0.5);

	this.instance_1 = new lib.CachedBmp_2122();
	this.instance_1.setTransform(-85.55,-24,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95,-26.7,190,53.5);


(lib.btnD = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.CachedBmp_2119();
	this.instance.setTransform(-52.9,-52.9,0.5,0.5);

	this.instance_1 = new lib.CachedBmp_2120();
	this.instance_1.setTransform(-47.8,-47.55,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-52.9,-52.9,106,106);


(lib.btnCong = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.CachedBmp_2333();
	this.instance.setTransform(-114.55,-23.9,0.5,0.5);

	this.instance_1 = new lib.CachedBmp_2334();
	this.instance_1.setTransform(-103.05,-21.5,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-114.5,-23.9,229,48);


(lib.btnCobalagi = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.CachedBmp_2331();
	this.instance.setTransform(-84.75,-30.8,0.5,0.5);

	this.instance_1 = new lib.CachedBmp_2332();
	this.instance_1.setTransform(-76.2,-27.7,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-84.7,-30.8,169.5,61.5);


(lib.btnC = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AiuCQQhBg4AAhYQAAhYBAg3QBCg5BsAAQCRAAA5BVQAdAqAAAqIhcAZQAEghgQghQgfhChfAAQhHAAgnAnQglAlAAA+QAABAAoAmQAoAlBDAAQBeAAAlg5QATgcgBgdIBbAYQgFAmghAmQhCBMiIAAQhrAAhDg5g");
	this.shape.setTransform(0.075,-3.175);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF9F48").s().p("Al1F2QibibAAjbQAAjaCbibQCbibDaAAQDbAACbCbQCbCbAADaQAADbibCbQibCbjbAAQjaAAibibg");
	this.shape_1.setTransform(0.025,0.025);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1,p:{scaleX:1,scaleY:1,x:0.025,y:0.025}},{t:this.shape,p:{scaleX:1,scaleY:1,x:0.075,y:-3.175}}]}).to({state:[{t:this.shape_1,p:{scaleX:0.8999,scaleY:0.8999,x:-0.2419,y:0.0037}},{t:this.shape,p:{scaleX:0.9,scaleY:0.9,x:-0.0149,y:-2.8328}}]},2).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-52.9,-52.9,105.9,105.9);


(lib.btnB = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.CachedBmp_2113();
	this.instance.setTransform(-52.9,-52.9,0.5,0.5);

	this.instance_1 = new lib.CachedBmp_2114();
	this.instance_1.setTransform(-48,-47.7,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-52.9,-52.9,106,106);


(lib.btnA = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.CachedBmp_2111();
	this.instance.setTransform(-52.9,-52.9,0.5,0.5);

	this.instance_1 = new lib.CachedBmp_2112();
	this.instance_1.setTransform(-48.1,-47.7,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-52.9,-52.9,106,106);


(lib.mcIntro = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// timeline functions:
	this.frame_0 = function() {
		createjs.Sound.play("intro")
	}
	this.frame_19 = function() {
		/* Stop at This Frame
		The  timeline will stop/pause at the frame where you insert this code.
		Can also be used to stop/pause the timeline of movieclips.
		*/
		
		this.stop();
		
		
		
		/* Click to Go to Frame and Play
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and continues playback from that frame.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		this.btnIntro.addEventListener("click", fl_ClickToGoToAndPlayFromFrame.bind(this));
		
		function fl_ClickToGoToAndPlayFromFrame()
		{
			createjs.Sound.stop("intro");
			createjs.Sound.play("bgm");
			this.gotoAndPlay(20);
		}
	}
	this.frame_39 = function() {
		exportRoot.gotoAndStop("home");
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(19).call(this.frame_19).wait(20).call(this.frame_39).wait(1));

	// Layer_2
	this.btnIntro = new lib.btnIntro();
	this.btnIntro.name = "btnIntro";
	this.btnIntro.setTransform(-303.9,-43.5);
	this.btnIntro.alpha = 0.25;
	this.btnIntro._off = true;
	new cjs.ButtonHelper(this.btnIntro, 0, 1, 2);

	this.timeline.addTween(cjs.Tween.get(this.btnIntro).wait(9).to({_off:false},0).to({y:-63.5,alpha:1},10,cjs.Ease.quadInOut).wait(1).to({x:-1510.2,alpha:0.25},19,cjs.Ease.quartInOut).wait(1));

	// Layer_1
	this.instance = new lib.grIntro("synched",0);
	this.instance.setTransform(0.45,-40);
	this.instance.alpha = 0.25;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:0,alpha:1},19,cjs.Ease.quadInOut).to({startPosition:0},1).to({x:-1205.85,alpha:0.25},19,cjs.Ease.quartInOut).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1575.6,-266.8,1945.1,493.5);


(lib.mcGuide = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// timeline functions:
	this.frame_19 = function() {
		/* Stop at This Frame
		The  timeline will stop/pause at the frame where you insert this code.
		Can also be used to stop/pause the timeline of movieclips.
		*/
		
		this.stop();
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnGuide.addEventListener("click", fl_ClickToGoToAndStopAtFrame_2.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_2()
		{
			exportRoot.gotoAndStop("intro");
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

	// Layer_2
	this.instance = new lib.grGuide("synched",0);
	this.instance.setTransform(0,-111.25);
	this.instance.alpha = 0.25;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:-71.25,alpha:1},14,cjs.Ease.quadInOut).to({startPosition:0},1).wait(5));

	// Layer_1
	this.btnGuide = new lib.btnGuide();
	this.btnGuide.name = "btnGuide";
	this.btnGuide.setTransform(-30.2,138.05);
	this.btnGuide.alpha = 0.25;
	this.btnGuide._off = true;
	new cjs.ButtonHelper(this.btnGuide, 0, 1, 2);

	this.timeline.addTween(cjs.Tween.get(this.btnGuide).wait(12).to({_off:false},0).to({alpha:1},7,cjs.Ease.quadInOut).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-510.9,-215.3,1022,390.6);


(lib.grHomechar = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.CachedBmp_2349();
	this.instance.setTransform(-564.6,-217.05,0.5,0.5);

	this.instance_1 = new lib.Group_12();
	this.instance_1.setTransform(403.25,212.4,1,1,0,0,0,108.1,4.8);
	this.instance_1.alpha = 0.5;
	this.instance_1.compositeOperation = "multiply";

	this.instance_2 = new lib.Group_13();
	this.instance_2.setTransform(-14.15,212.4,1,1,0,0,0,112.5,4.8);
	this.instance_2.alpha = 0.5;
	this.instance_2.compositeOperation = "multiply";

	this.instance_3 = new lib.Group_14();
	this.instance_3.setTransform(-443.6,212.4,1,1,0,0,0,97,4.8);
	this.instance_3.alpha = 0.5;
	this.instance_3.compositeOperation = "multiply";

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-564.6,-217,1129,434.1);


(lib.btnQuiz = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.CachedBmp_2347();
	this.instance.setTransform(-54,-19.15,0.5,0.5);

	this.instance_1 = new lib.Group_2();
	this.instance_1.setTransform(0,0.05,1,1,0,0,0,53,18.2);
	this.instance_1.alpha = 0.0117;

	this.instance_2 = new lib.CachedBmp_2348();
	this.instance_2.setTransform(-48.6,-17.2,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1,p:{regX:53,scaleX:1,scaleY:1,x:0}},{t:this.instance}]}).to({state:[{t:this.instance_1,p:{regX:53.1,scaleX:0.9,scaleY:0.9,x:0.05}},{t:this.instance_2}]},2).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-54,-19.1,108,38.5);


(lib.btnPrev = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.CachedBmp_2345();
	this.instance.setTransform(-38.75,-38.7,0.5,0.5);

	this.instance_1 = new lib.Path();
	this.instance_1.setTransform(0,0.05,1,1,0,0,0,35,35);
	this.instance_1.alpha = 0.1016;

	this.instance_2 = new lib.CachedBmp_2346();
	this.instance_2.setTransform(-34.85,-34.8,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1,p:{regY:35,scaleX:1,scaleY:1,x:0}},{t:this.instance}]}).to({state:[{t:this.instance_1,p:{regY:35.1,scaleX:0.9,scaleY:0.9,x:-0.05}},{t:this.instance_2}]},2).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-38.7,-38.7,77.5,77.5);


(lib.btnNext = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.CachedBmp_2343();
	this.instance.setTransform(-38.75,-38.7,0.5,0.5);

	this.instance_1 = new lib.Path_0();
	this.instance_1.setTransform(0.05,0.05,1,1,0,0,0,34.8,34.8);
	this.instance_1.alpha = 0.1016;

	this.instance_2 = new lib.CachedBmp_2344();
	this.instance_2.setTransform(-34.85,-34.8,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1,p:{scaleX:1,scaleY:1,x:0.05,y:0.05}},{t:this.instance}]}).to({state:[{t:this.instance_1,p:{scaleX:0.8999,scaleY:0.8999,x:-0.1,y:0}},{t:this.instance_2}]},2).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-38.7,-38.7,77.5,77.5);


(lib.btnGlossary = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.Group();
	this.instance.setTransform(0.05,0.05,1,1,0,0,0,68.5,18.2);
	this.instance.alpha = 0;

	this.instance_1 = new lib.CachedBmp_2335();
	this.instance_1.setTransform(-69.45,-19.15,0.5,0.5);

	this.instance_2 = new lib.CachedBmp_2336();
	this.instance_2.setTransform(-62.5,-17.2,0.5,0.5);

	this.instance_3 = new lib.CachedBmp_2338();
	this.instance_3.setTransform(-68,-17.75,0.5,0.5);

	this.instance_4 = new lib.CachedBmp_2337();
	this.instance_4.setTransform(-69.45,-19.15,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance,p:{scaleX:1,scaleY:1,x:0.05}}]}).to({state:[{t:this.instance_2},{t:this.instance,p:{scaleX:0.9,scaleY:0.9,x:-0.05}}]},2).to({state:[{t:this.instance_4},{t:this.instance,p:{scaleX:1,scaleY:1,x:0.05}},{t:this.instance_3}]},1).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-69.4,-19.1,139,38.5);


(lib.Tween4 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.btnGlossary = new lib.btnGlossary();
	this.btnGlossary.name = "btnGlossary";
	this.btnGlossary.setTransform(-200.6,-304.55);
	new cjs.ButtonHelper(this.btnGlossary, 0, 1, 2, false, new lib.btnGlossary(), 3);

	this.btnQuiz = new lib.btnQuiz();
	this.btnQuiz.name = "btnQuiz";
	this.btnQuiz.setTransform(-334.05,-304.55);
	new cjs.ButtonHelper(this.btnQuiz, 0, 1, 2);

	this.instance = new lib.grHomeinfo("synched",0);
	this.instance.setTransform(448.35,322);

	this.instance_1 = new lib.grHomelogo("synched",0);
	this.instance_1.setTransform(-498.1,-300.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance},{t:this.btnQuiz},{t:this.btnGlossary}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-581.1,-324.1,1162.3000000000002,655);


(lib.Tween3 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.mcSound = new lib.mcSound();
	this.mcSound.name = "mcSound";
	this.mcSound.setTransform(543,-300.6);

	this.btnGlossary = new lib.btnGlossary();
	this.btnGlossary.name = "btnGlossary";
	this.btnGlossary.setTransform(-200.6,-304.55);
	new cjs.ButtonHelper(this.btnGlossary, 0, 1, 2, false, new lib.btnGlossary(), 3);

	this.btnQuiz = new lib.btnQuiz();
	this.btnQuiz.name = "btnQuiz";
	this.btnQuiz.setTransform(-334.05,-304.55);
	new cjs.ButtonHelper(this.btnQuiz, 0, 1, 2);

	this.instance = new lib.grHomeinfo("synched",0);
	this.instance.setTransform(448.35,322);

	this.instance_1 = new lib.grHomelogo("synched",0);
	this.instance_1.setTransform(-498.1,-300.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance},{t:this.btnQuiz},{t:this.btnGlossary},{t:this.mcSound}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-581.1,-324.1,1162.3000000000002,655);


(lib.Tween2 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.btnFuture = new lib.btnFuture();
	this.btnFuture.name = "btnFuture";
	this.btnFuture.setTransform(405.5,225.4);
	new cjs.ButtonHelper(this.btnFuture, 0, 1, 2);

	this.btnPresent = new lib.btnPresent();
	this.btnPresent.name = "btnPresent";
	this.btnPresent.setTransform(-27.5,224.4);
	new cjs.ButtonHelper(this.btnPresent, 0, 1, 2);

	this.btnPast = new lib.btnPast();
	this.btnPast.name = "btnPast";
	this.btnPast.setTransform(-454.8,225.65);
	new cjs.ButtonHelper(this.btnPast, 0, 1, 2);

	this.instance = new lib.grHomechar("synched",0);
	this.instance.setTransform(0,-35.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.btnPast},{t:this.btnPresent},{t:this.btnFuture}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-564.6,-252.3,1129,504.8);


(lib.Tween1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.btnFuture = new lib.btnFuture();
	this.btnFuture.name = "btnFuture";
	this.btnFuture.setTransform(405.5,225.4);
	new cjs.ButtonHelper(this.btnFuture, 0, 1, 2);

	this.btnPresent = new lib.btnPresent();
	this.btnPresent.name = "btnPresent";
	this.btnPresent.setTransform(-27.5,224.4);
	new cjs.ButtonHelper(this.btnPresent, 0, 1, 2);

	this.btnPast = new lib.btnPast();
	this.btnPast.name = "btnPast";
	this.btnPast.setTransform(-454.8,225.65);
	new cjs.ButtonHelper(this.btnPast, 0, 1, 2);

	this.instance = new lib.grHomechar("synched",0);
	this.instance.setTransform(0,-35.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.btnPast},{t:this.btnPresent},{t:this.btnFuture}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-564.6,-252.3,1129,504.8);


(lib.mcHome = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// timeline functions:
	this.frame_30 = function() {
		/* Stop at This Frame
		The  timeline will stop/pause at the frame where you insert this code.
		Can also be used to stop/pause the timeline of movieclips.
		*/
		
		this.stop();
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnPast.addEventListener("click", fl_ClickToGoToAndStopAtFrame_4.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_4()
		{
			exportRoot.gotoAndStop("pt1");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnPresent.addEventListener("click", fl_ClickToGoToAndStopAtFrame_5.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_5()
		{
			exportRoot.gotoAndStop("pr1");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnFuture.addEventListener("click", fl_ClickToGoToAndStopAtFrame_6.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_6()
		{
			exportRoot.gotoAndStop("ft1");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnQuiz.addEventListener("click", fl_ClickToGoToAndStopAtFrame_7.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_7()
		{
			exportRoot.gotoAndStop("q1");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnGlossary.addEventListener("click", fl_ClickToGoToAndStopAtFrame_8.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_8()
		{
			exportRoot.gotoAndStop("g1");
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(30).call(this.frame_30).wait(1));

	// Layer_3
	this.instance = new lib.Tween3("synched",0);
	this.instance.setTransform(3.1,40.75);
	this.instance.alpha = 0.25;
	this.instance._off = true;

	this.instance_1 = new lib.Tween4("synched",0);
	this.instance_1.setTransform(3.1,0.75);
	this.instance_1._off = true;

	this.mcSound = new lib.mcSound();
	this.mcSound.name = "mcSound";
	this.mcSound.setTransform(546.1,-299.85);

	this.btnGlossary = new lib.btnGlossary();
	this.btnGlossary.name = "btnGlossary";
	this.btnGlossary.setTransform(-197.5,-303.8);
	new cjs.ButtonHelper(this.btnGlossary, 0, 1, 2, false, new lib.btnGlossary(), 3);

	this.btnQuiz = new lib.btnQuiz();
	this.btnQuiz.name = "btnQuiz";
	this.btnQuiz.setTransform(-330.95,-303.8);
	new cjs.ButtonHelper(this.btnQuiz, 0, 1, 2);

	this.instance_2 = new lib.grHomeinfo("synched",0);
	this.instance_2.setTransform(451.45,322.75);

	this.instance_3 = new lib.grHomelogo("synched",0);
	this.instance_3.setTransform(-495,-299.95);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance}]},19).to({state:[{t:this.instance_1}]},10).to({state:[{t:this.instance_3},{t:this.instance_2},{t:this.btnQuiz},{t:this.btnGlossary},{t:this.mcSound}]},1).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance).wait(19).to({_off:false},0).to({_off:true,y:0.75,alpha:1},10,cjs.Ease.quartOut).wait(2));
	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(19).to({_off:false},10,cjs.Ease.quartOut).to({_off:true,x:546.1,y:-299.85,mode:"independent"},1).wait(1));

	// Layer_1
	this.instance_4 = new lib.Tween1("synched",0);
	this.instance_4.setTransform(-1206.35,1.3);

	this.instance_5 = new lib.Tween2("synched",0);
	this.instance_5.setTransform(7.95,1.3);
	this.instance_5._off = true;

	this.btnFuture = new lib.btnFuture();
	this.btnFuture.name = "btnFuture";
	this.btnFuture.setTransform(413.45,226.7);
	new cjs.ButtonHelper(this.btnFuture, 0, 1, 2);

	this.btnPresent = new lib.btnPresent();
	this.btnPresent.name = "btnPresent";
	this.btnPresent.setTransform(-19.55,225.7);
	new cjs.ButtonHelper(this.btnPresent, 0, 1, 2);

	this.btnPast = new lib.btnPast();
	this.btnPast.name = "btnPast";
	this.btnPast.setTransform(-446.85,226.95);
	new cjs.ButtonHelper(this.btnPast, 0, 1, 2);

	this.instance_6 = new lib.grHomechar("synched",0);
	this.instance_6.setTransform(7.95,-34);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_4}]}).to({state:[{t:this.instance_5}]},29).to({state:[{t:this.instance_6},{t:this.btnPast},{t:this.btnPresent},{t:this.btnFuture}]},1).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({_off:true,x:7.95},29,cjs.Ease.quartInOut).wait(2));
	this.timeline.addTween(cjs.Tween.get(this.instance_5).to({_off:false},29,cjs.Ease.quartInOut).to({_off:true,x:413.45,y:226.7,mode:"independent"},1).wait(1));

	// Layer_2
	this.instance_7 = new lib.grHomebg("synched",0);
	this.instance_7.setTransform(936.2,0);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).to({x:0},29,cjs.Ease.cubicInOut).to({startPosition:0},1).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1770.9,-360,3347.1000000000004,731.6);


// stage content:
(lib.TAArdhi = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {"Main Page":0,"Past Tense":3,"Present Tense":7,"Future Tense":11,"Quiz Page":15,"Glossary Page":25,"Response Page":27,guide:0,intro:1,home:2,pt1:3,pt2:4,pt3:5,pt4:6,pr1:7,pr2:8,pr3:9,pr4:10,ft1:11,ft2:12,ft3:13,ft4:14,q1:15,q2:16,q3:17,q4:18,q5:19,q6:20,q7:21,q8:22,q9:23,q10:24,g1:25,g2:26,r1:27,r2:28,r3:29,r4:30,r5:31,r6:32,r7:33,r8:34,r9:35,r10:36,w1:37,w2:38,w3:39,w4:40,w5:41,w6:42,w7:43,w8:44,w9:45,w10:46,cong:47};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	this.actionFrames = [0,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47];
	// timeline functions:
	this.frame_0 = function() {
		/* Stop at This Frame
		The  timeline will stop/pause at the frame where you insert this code.
		Can also be used to stop/pause the timeline of movieclips.
		*/
		
		this.stop();
	}
	this.frame_3 = function() {
		this.btnNext1.addEventListener("click", fl_ClickToGoToAndStopAtFrame_11.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_11()
		{
			this.gotoAndStop("pt2");
		}
		
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnHome1.addEventListener("click", fl_ClickToGoToAndStopAtFrame_96.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_96()
		{
			this.gotoAndStop("home");
		}
	}
	this.frame_4 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnPrev2.addEventListener("click", fl_ClickToGoToAndStopAtFrame_13.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_13()
		{
			this.gotoAndStop("pt1");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnNext2.addEventListener("click", fl_ClickToGoToAndStopAtFrame_14.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_14()
		{
			this.gotoAndStop("pt3");
		}
	}
	this.frame_5 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnPrev3.addEventListener("click", fl_ClickToGoToAndStopAtFrame_15.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_15()
		{
			this.gotoAndStop("pt2");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnNext3.addEventListener("click", fl_ClickToGoToAndStopAtFrame_16.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_16()
		{
			this.gotoAndStop("pt4");
		}
	}
	this.frame_6 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnPrev4.addEventListener("click", fl_ClickToGoToAndStopAtFrame_17.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_17()
		{
			this.gotoAndStop("pt3");
		}
	}
	this.frame_7 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnNext5.addEventListener("click", fl_ClickToGoToAndStopAtFrame_18.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_18()
		{
			this.gotoAndStop("pr2");
		}
	}
	this.frame_8 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnPrev6.addEventListener("click", fl_ClickToGoToAndStopAtFrame_19.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_19()
		{
			this.gotoAndStop("pr1");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnNext6.addEventListener("click", fl_ClickToGoToAndStopAtFrame_20.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_20()
		{
			this.gotoAndStop("pr3");
		}
	}
	this.frame_9 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnPrev7.addEventListener("click", fl_ClickToGoToAndStopAtFrame_21.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_21()
		{
			this.gotoAndStop("pr2");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnNext7.addEventListener("click", fl_ClickToGoToAndStopAtFrame_22.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_22()
		{
			this.gotoAndStop("pr4");
		}
	}
	this.frame_10 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnPrev8.addEventListener("click", fl_ClickToGoToAndStopAtFrame_23.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_23()
		{
			this.gotoAndStop("pr3");
		}
	}
	this.frame_11 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnNext9.addEventListener("click", fl_ClickToGoToAndStopAtFrame_24.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_24()
		{
			this.gotoAndStop("ft2");
		}
	}
	this.frame_12 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnPrev10.addEventListener("click", fl_ClickToGoToAndStopAtFrame_25.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_25()
		{
			this.gotoAndStop("ft1");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnNext10.addEventListener("click", fl_ClickToGoToAndStopAtFrame_26.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_26()
		{
			this.gotoAndStop("ft3");
		}
	}
	this.frame_13 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnPrev11.addEventListener("click", fl_ClickToGoToAndStopAtFrame_27.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_27()
		{
			this.gotoAndStop("ft2");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnNext11.addEventListener("click", fl_ClickToGoToAndStopAtFrame_28.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_28()
		{
			this.gotoAndStop("ft4");
		}
	}
	this.frame_14 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnPrev12.addEventListener("click", fl_ClickToGoToAndStopAtFrame_29.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_29()
		{
			this.gotoAndStop("ft3");
		}
	}
	this.frame_15 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnHome2.addEventListener("click", fl_ClickToGoToAndStopAtFrame_10.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_10()
		{
			this.gotoAndStop("home");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnD1.addEventListener("click", fl_ClickToGoToAndStopAtFrame_30.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_30()
		{
			this.gotoAndStop("r1");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnA1.addEventListener("click", fl_ClickToGoToAndStopAtFrame_40.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_40()
		{
			this.gotoAndStop("w1");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnB1.addEventListener("click", fl_ClickToGoToAndStopAtFrame_41.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_41()
		{
			this.gotoAndStop("w1");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnC1.addEventListener("click", fl_ClickToGoToAndStopAtFrame_42.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_42()
		{
			this.gotoAndStop("w1");
		}
	}
	this.frame_16 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		
		this.btnC2.addEventListener("click", fl_ClickToGoToAndStopAtFrame_31.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_31()
		{
			this.gotoAndStop("r2");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnD2.addEventListener("click", fl_ClickToGoToAndStopAtFrame_43.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_43()
		{
			this.gotoAndStop("w2");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnB2.addEventListener("click", fl_ClickToGoToAndStopAtFrame_44.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_44()
		{
			this.gotoAndStop("w2");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnA2.addEventListener("click", fl_ClickToGoToAndStopAtFrame_45.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_45()
		{
			this.gotoAndStop("w2");
		}
	}
	this.frame_17 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnD3.addEventListener("click", fl_ClickToGoToAndStopAtFrame_32.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_32()
		{
			this.gotoAndStop("r3");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnC3.addEventListener("click", fl_ClickToGoToAndStopAtFrame_46.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_46()
		{
			this.gotoAndStop("w3");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnB3.addEventListener("click", fl_ClickToGoToAndStopAtFrame_47.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_47()
		{
			this.gotoAndStop("w3");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnA3.addEventListener("click", fl_ClickToGoToAndStopAtFrame_48.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_48()
		{
			this.gotoAndStop("w3");
		}
	}
	this.frame_18 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnD4.addEventListener("click", fl_ClickToGoToAndStopAtFrame_33.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_33()
		{
			this.gotoAndStop("r4");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnC4.addEventListener("click", fl_ClickToGoToAndStopAtFrame_49.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_49()
		{
			this.gotoAndStop("w4");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnB4.addEventListener("click", fl_ClickToGoToAndStopAtFrame_50.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_50()
		{
			this.gotoAndStop("w4");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnA4.addEventListener("click", fl_ClickToGoToAndStopAtFrame_51.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_51()
		{
			this.gotoAndStop("w4");
		}
	}
	this.frame_19 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnA5.addEventListener("click", fl_ClickToGoToAndStopAtFrame_34.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_34()
		{
			this.gotoAndStop("r5");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnB5.addEventListener("click", fl_ClickToGoToAndStopAtFrame_52.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_52()
		{
			this.gotoAndStop("w5");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnC5.addEventListener("click", fl_ClickToGoToAndStopAtFrame_53.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_53()
		{
			this.gotoAndStop("w5");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnD5.addEventListener("click", fl_ClickToGoToAndStopAtFrame_54.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_54()
		{
			this.gotoAndStop("w5");
		}
	}
	this.frame_20 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnC6.addEventListener("click", fl_ClickToGoToAndStopAtFrame_35.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_35()
		{
			this.gotoAndStop("r6");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnD6.addEventListener("click", fl_ClickToGoToAndStopAtFrame_55.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_55()
		{
			this.gotoAndStop("w6");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnB6.addEventListener("click", fl_ClickToGoToAndStopAtFrame_56.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_56()
		{
			this.gotoAndStop("w6");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnA6.addEventListener("click", fl_ClickToGoToAndStopAtFrame_57.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_57()
		{
			this.gotoAndStop("w6");
		}
	}
	this.frame_21 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnC7.addEventListener("click", fl_ClickToGoToAndStopAtFrame_36.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_36()
		{
			this.gotoAndStop("r7");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnD7.addEventListener("click", fl_ClickToGoToAndStopAtFrame_58.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_58()
		{
			this.gotoAndStop("w7");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnB7.addEventListener("click", fl_ClickToGoToAndStopAtFrame_59.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_59()
		{
			this.gotoAndStop("w7");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnA7.addEventListener("click", fl_ClickToGoToAndStopAtFrame_60.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_60()
		{
			this.gotoAndStop("w7");
		}
	}
	this.frame_22 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnA8.addEventListener("click", fl_ClickToGoToAndStopAtFrame_37.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_37()
		{
			this.gotoAndStop("r8");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnB8.addEventListener("click", fl_ClickToGoToAndStopAtFrame_61.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_61()
		{
			this.gotoAndStop("w8");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnC8.addEventListener("click", fl_ClickToGoToAndStopAtFrame_62.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_62()
		{
			this.gotoAndStop("w8");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnD8.addEventListener("click", fl_ClickToGoToAndStopAtFrame_63.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_63()
		{
			this.gotoAndStop("w8");
		}
	}
	this.frame_23 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnC9.addEventListener("click", fl_ClickToGoToAndStopAtFrame_38.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_38()
		{
			this.gotoAndStop("r9");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnD9.addEventListener("click", fl_ClickToGoToAndStopAtFrame_64.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_64()
		{
			this.gotoAndStop("w9");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnB9.addEventListener("click", fl_ClickToGoToAndStopAtFrame_65.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_65()
		{
			this.gotoAndStop("w9");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnA9.addEventListener("click", fl_ClickToGoToAndStopAtFrame_66.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_66()
		{
			this.gotoAndStop("w9");
		}
	}
	this.frame_24 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnB10.addEventListener("click", fl_ClickToGoToAndStopAtFrame_39.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_39()
		{
			this.gotoAndStop("cong");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnA10.addEventListener("click", fl_ClickToGoToAndStopAtFrame_67.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_67()
		{
			this.gotoAndStop("w10");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnC10.addEventListener("click", fl_ClickToGoToAndStopAtFrame_68.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_68()
		{
			this.gotoAndStop("w10");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnD10.addEventListener("click", fl_ClickToGoToAndStopAtFrame_69.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_69()
		{
			this.gotoAndStop("w10");
		}
	}
	this.frame_25 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnNext13.addEventListener("click", fl_ClickToGoToAndStopAtFrame_90.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_90()
		{
			this.gotoAndStop("g2");
		}
		
		
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnHome3.addEventListener("click", fl_ClickToGoToAndStopAtFrame_94.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_94()
		{
			this.gotoAndStop("home");
		}
	}
	this.frame_26 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		
		this.btnPrev14.addEventListener("click", fl_ClickToGoToAndStopAtFrame_91.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_91()
		{
			this.gotoAndStop("g1");
		}
	}
	this.frame_27 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		createjs.Sound.play("correct")
		
		this.btnLanjut1.addEventListener("click", fl_ClickToGoToAndStopAtFrame_70.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_70()
		{
			this.gotoAndStop("q2");
		}
	}
	this.frame_28 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		createjs.Sound.play("correct")
		
		this.btnLanjut2.addEventListener("click", fl_ClickToGoToAndStopAtFrame_71.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_71()
		{
			this.gotoAndStop("q3");
		}
	}
	this.frame_29 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		createjs.Sound.play("correct")
		
		this.btnLanjut3.addEventListener("click", fl_ClickToGoToAndStopAtFrame_72.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_72()
		{
			this.gotoAndStop("q4");
		}
	}
	this.frame_30 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		createjs.Sound.play("correct")
		
		
		this.btnLanjut4.addEventListener("click", fl_ClickToGoToAndStopAtFrame_73.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_73()
		{
			this.gotoAndStop("q5");
		}
	}
	this.frame_31 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		createjs.Sound.play("correct")
		
		this.btnLanjut5.addEventListener("click", fl_ClickToGoToAndStopAtFrame_74.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_74()
		{
			this.gotoAndStop("q6");
		}
	}
	this.frame_32 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		createjs.Sound.play("correct")
		
		this.btnLanjut6.addEventListener("click", fl_ClickToGoToAndStopAtFrame_75.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_75()
		{
			this.gotoAndStop("q7");
		}
	}
	this.frame_33 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		createjs.Sound.play("correct")
		
		this.btnLanjut7.addEventListener("click", fl_ClickToGoToAndStopAtFrame_76.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_76()
		{
			this.gotoAndStop("q8");
		}
	}
	this.frame_34 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		createjs.Sound.play("correct")
		
		this.btnLanjut8.addEventListener("click", fl_ClickToGoToAndStopAtFrame_77.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_77()
		{
			this.gotoAndStop("q9");
		}
	}
	this.frame_35 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		createjs.Sound.play("correct")
		
		this.btnLanjut9.addEventListener("click", fl_ClickToGoToAndStopAtFrame_78.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_78()
		{
			this.gotoAndStop("q10");
		}
	}
	this.frame_36 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		createjs.Sound.play("correct")
		
		this.btnLanjut10.addEventListener("click", fl_ClickToGoToAndStopAtFrame_79.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_79()
		{
			this.gotoAndStop("cong");
		}
	}
	this.frame_37 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		createjs.Sound.play("wrong")
		
		this.btnCobalagi1.addEventListener("click", fl_ClickToGoToAndStopAtFrame_80.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_80()
		{
			this.gotoAndStop("q1");
		}
	}
	this.frame_38 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		createjs.Sound.play("wrong")
		
		
		this.btnCobalagi2.addEventListener("click", fl_ClickToGoToAndStopAtFrame_81.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_81()
		{
			this.gotoAndStop("q2");
		}
	}
	this.frame_39 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		createjs.Sound.play("wrong")
		
		
		this.btnCobalagi3.addEventListener("click", fl_ClickToGoToAndStopAtFrame_82.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_82()
		{
			this.gotoAndStop("q3");
		}
	}
	this.frame_40 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		createjs.Sound.play("wrong")
		
		
		this.btnCobalagi4.addEventListener("click", fl_ClickToGoToAndStopAtFrame_83.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_83()
		{
			this.gotoAndStop("q4");
		}
	}
	this.frame_41 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		createjs.Sound.play("wrong")
		
		
		this.btnCobalagi5.addEventListener("click", fl_ClickToGoToAndStopAtFrame_84.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_84()
		{
			this.gotoAndStop("q5");
		}
	}
	this.frame_42 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		createjs.Sound.play("wrong")
		
		
		this.btnCobalagi6.addEventListener("click", fl_ClickToGoToAndStopAtFrame_85.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_85()
		{
			this.gotoAndStop("q6");
		}
	}
	this.frame_43 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		createjs.Sound.play("wrong")
		
		
		this.btnCobalagi7.addEventListener("click", fl_ClickToGoToAndStopAtFrame_86.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_86()
		{
			this.gotoAndStop("q7");
		}
	}
	this.frame_44 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		createjs.Sound.play("wrong")
		
		
		this.btnCobalagi8.addEventListener("click", fl_ClickToGoToAndStopAtFrame_87.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_87()
		{
			this.gotoAndStop("q8");
		}
	}
	this.frame_45 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		createjs.Sound.play("wrong")
		
		
		this.btnCobalagi9.addEventListener("click", fl_ClickToGoToAndStopAtFrame_88.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_88()
		{
			this.gotoAndStop("q9");
		}
	}
	this.frame_46 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		createjs.Sound.play("wrong")
		
		
		this.btnCobalagi10.addEventListener("click", fl_ClickToGoToAndStopAtFrame_89.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_89()
		{
			this.gotoAndStop("q10");
		}
	}
	this.frame_47 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		createjs.Sound.play("cong")
		
		this.btnCong.addEventListener("click", fl_ClickToGoToAndStopAtFrame_92.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame_92()
		{
			this.gotoAndStop("home");
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(3).call(this.frame_3).wait(1).call(this.frame_4).wait(1).call(this.frame_5).wait(1).call(this.frame_6).wait(1).call(this.frame_7).wait(1).call(this.frame_8).wait(1).call(this.frame_9).wait(1).call(this.frame_10).wait(1).call(this.frame_11).wait(1).call(this.frame_12).wait(1).call(this.frame_13).wait(1).call(this.frame_14).wait(1).call(this.frame_15).wait(1).call(this.frame_16).wait(1).call(this.frame_17).wait(1).call(this.frame_18).wait(1).call(this.frame_19).wait(1).call(this.frame_20).wait(1).call(this.frame_21).wait(1).call(this.frame_22).wait(1).call(this.frame_23).wait(1).call(this.frame_24).wait(1).call(this.frame_25).wait(1).call(this.frame_26).wait(1).call(this.frame_27).wait(1).call(this.frame_28).wait(1).call(this.frame_29).wait(1).call(this.frame_30).wait(1).call(this.frame_31).wait(1).call(this.frame_32).wait(1).call(this.frame_33).wait(1).call(this.frame_34).wait(1).call(this.frame_35).wait(1).call(this.frame_36).wait(1).call(this.frame_37).wait(1).call(this.frame_38).wait(1).call(this.frame_39).wait(1).call(this.frame_40).wait(1).call(this.frame_41).wait(1).call(this.frame_42).wait(1).call(this.frame_43).wait(1).call(this.frame_44).wait(1).call(this.frame_45).wait(1).call(this.frame_46).wait(1).call(this.frame_47).wait(1));

	// Home
	this.btnHome1 = new lib.btnHome1();
	this.btnHome1.name = "btnHome1";
	this.btnHome1.setTransform(1192,382.65);
	new cjs.ButtonHelper(this.btnHome1, 0, 1, 2);

	this.btnHome2 = new lib.btnHome2();
	this.btnHome2.name = "btnHome2";
	this.btnHome2.setTransform(119.65,93.55);
	new cjs.ButtonHelper(this.btnHome2, 0, 1, 2);

	this.btnHome3 = new lib.btnHome3();
	this.btnHome3.name = "btnHome3";
	this.btnHome3.setTransform(1192,382.65);
	new cjs.ButtonHelper(this.btnHome3, 0, 1, 2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.btnHome1}]},3).to({state:[{t:this.btnHome2}]},12).to({state:[{t:this.btnHome3}]},10).to({state:[]},2).wait(21));

	// Navigation
	this.btnNext1 = new lib.btnNext();
	this.btnNext1.name = "btnNext1";
	this.btnNext1.setTransform(1191.05,500.9);
	new cjs.ButtonHelper(this.btnNext1, 0, 1, 2);

	this.btnPrev2 = new lib.btnPrev();
	this.btnPrev2.name = "btnPrev2";
	this.btnPrev2.setTransform(1191.05,265.45);
	new cjs.ButtonHelper(this.btnPrev2, 0, 1, 2);

	this.btnNext2 = new lib.btnNext();
	this.btnNext2.name = "btnNext2";
	this.btnNext2.setTransform(1191.05,500.9);
	new cjs.ButtonHelper(this.btnNext2, 0, 1, 2);

	this.btnPrev3 = new lib.btnPrev();
	this.btnPrev3.name = "btnPrev3";
	this.btnPrev3.setTransform(1191.05,265.45);
	new cjs.ButtonHelper(this.btnPrev3, 0, 1, 2);

	this.btnNext3 = new lib.btnNext();
	this.btnNext3.name = "btnNext3";
	this.btnNext3.setTransform(1191.05,500.9);
	new cjs.ButtonHelper(this.btnNext3, 0, 1, 2);

	this.btnPrev4 = new lib.btnPrev();
	this.btnPrev4.name = "btnPrev4";
	this.btnPrev4.setTransform(1191.05,265.45);
	new cjs.ButtonHelper(this.btnPrev4, 0, 1, 2);

	this.btnNext5 = new lib.btnNext();
	this.btnNext5.name = "btnNext5";
	this.btnNext5.setTransform(1191.05,500.9);
	new cjs.ButtonHelper(this.btnNext5, 0, 1, 2);

	this.btnPrev6 = new lib.btnPrev();
	this.btnPrev6.name = "btnPrev6";
	this.btnPrev6.setTransform(1191.05,265.45);
	new cjs.ButtonHelper(this.btnPrev6, 0, 1, 2);

	this.btnNext6 = new lib.btnNext();
	this.btnNext6.name = "btnNext6";
	this.btnNext6.setTransform(1191.05,500.9);
	new cjs.ButtonHelper(this.btnNext6, 0, 1, 2);

	this.btnPrev7 = new lib.btnPrev();
	this.btnPrev7.name = "btnPrev7";
	this.btnPrev7.setTransform(1191.05,265.45);
	new cjs.ButtonHelper(this.btnPrev7, 0, 1, 2);

	this.btnNext7 = new lib.btnNext();
	this.btnNext7.name = "btnNext7";
	this.btnNext7.setTransform(1191.05,500.9);
	new cjs.ButtonHelper(this.btnNext7, 0, 1, 2);

	this.btnPrev8 = new lib.btnPrev();
	this.btnPrev8.name = "btnPrev8";
	this.btnPrev8.setTransform(1191.05,265.45);
	new cjs.ButtonHelper(this.btnPrev8, 0, 1, 2);

	this.btnNext9 = new lib.btnNext();
	this.btnNext9.name = "btnNext9";
	this.btnNext9.setTransform(1191.05,500.9);
	new cjs.ButtonHelper(this.btnNext9, 0, 1, 2);

	this.btnPrev10 = new lib.btnPrev();
	this.btnPrev10.name = "btnPrev10";
	this.btnPrev10.setTransform(1191.05,265.45);
	new cjs.ButtonHelper(this.btnPrev10, 0, 1, 2);

	this.btnNext10 = new lib.btnNext();
	this.btnNext10.name = "btnNext10";
	this.btnNext10.setTransform(1191.05,500.9);
	new cjs.ButtonHelper(this.btnNext10, 0, 1, 2);

	this.btnPrev11 = new lib.btnPrev();
	this.btnPrev11.name = "btnPrev11";
	this.btnPrev11.setTransform(1191.05,265.45);
	new cjs.ButtonHelper(this.btnPrev11, 0, 1, 2);

	this.btnNext11 = new lib.btnNext();
	this.btnNext11.name = "btnNext11";
	this.btnNext11.setTransform(1191.05,500.9);
	new cjs.ButtonHelper(this.btnNext11, 0, 1, 2);

	this.btnPrev12 = new lib.btnPrev();
	this.btnPrev12.name = "btnPrev12";
	this.btnPrev12.setTransform(1191.05,265.45);
	new cjs.ButtonHelper(this.btnPrev12, 0, 1, 2);

	this.btnD1 = new lib.btnD();
	this.btnD1.name = "btnD1";
	this.btnD1.setTransform(874.15,599.55);
	new cjs.ButtonHelper(this.btnD1, 0, 1, 2);

	this.btnC1 = new lib.btnC();
	this.btnC1.name = "btnC1";
	this.btnC1.setTransform(714.3,599.55);
	new cjs.ButtonHelper(this.btnC1, 0, 1, 2);

	this.btnB1 = new lib.btnB();
	this.btnB1.name = "btnB1";
	this.btnB1.setTransform(555.5,599.55);
	new cjs.ButtonHelper(this.btnB1, 0, 1, 2);

	this.btnA1 = new lib.btnA();
	this.btnA1.name = "btnA1";
	this.btnA1.setTransform(396.7,599.55);
	new cjs.ButtonHelper(this.btnA1, 0, 1, 2);

	this.btnD2 = new lib.btnD();
	this.btnD2.name = "btnD2";
	this.btnD2.setTransform(874.15,599.55);
	new cjs.ButtonHelper(this.btnD2, 0, 1, 2);

	this.btnC2 = new lib.btnC();
	this.btnC2.name = "btnC2";
	this.btnC2.setTransform(714.3,599.55);
	new cjs.ButtonHelper(this.btnC2, 0, 1, 2);

	this.btnB2 = new lib.btnB();
	this.btnB2.name = "btnB2";
	this.btnB2.setTransform(555.5,599.55);
	new cjs.ButtonHelper(this.btnB2, 0, 1, 2);

	this.btnA2 = new lib.btnA();
	this.btnA2.name = "btnA2";
	this.btnA2.setTransform(396.7,599.55);
	new cjs.ButtonHelper(this.btnA2, 0, 1, 2);

	this.btnD3 = new lib.btnD();
	this.btnD3.name = "btnD3";
	this.btnD3.setTransform(874.15,599.55);
	new cjs.ButtonHelper(this.btnD3, 0, 1, 2);

	this.btnC3 = new lib.btnC();
	this.btnC3.name = "btnC3";
	this.btnC3.setTransform(714.3,599.55);
	new cjs.ButtonHelper(this.btnC3, 0, 1, 2);

	this.btnB3 = new lib.btnB();
	this.btnB3.name = "btnB3";
	this.btnB3.setTransform(555.5,599.55);
	new cjs.ButtonHelper(this.btnB3, 0, 1, 2);

	this.btnA3 = new lib.btnA();
	this.btnA3.name = "btnA3";
	this.btnA3.setTransform(396.7,599.55);
	new cjs.ButtonHelper(this.btnA3, 0, 1, 2);

	this.btnD4 = new lib.btnD();
	this.btnD4.name = "btnD4";
	this.btnD4.setTransform(874.15,599.55);
	new cjs.ButtonHelper(this.btnD4, 0, 1, 2);

	this.btnC4 = new lib.btnC();
	this.btnC4.name = "btnC4";
	this.btnC4.setTransform(714.3,599.55);
	new cjs.ButtonHelper(this.btnC4, 0, 1, 2);

	this.btnB4 = new lib.btnB();
	this.btnB4.name = "btnB4";
	this.btnB4.setTransform(555.5,599.55);
	new cjs.ButtonHelper(this.btnB4, 0, 1, 2);

	this.btnA4 = new lib.btnA();
	this.btnA4.name = "btnA4";
	this.btnA4.setTransform(396.7,599.55);
	new cjs.ButtonHelper(this.btnA4, 0, 1, 2);

	this.btnD5 = new lib.btnD();
	this.btnD5.name = "btnD5";
	this.btnD5.setTransform(874.15,599.55);
	new cjs.ButtonHelper(this.btnD5, 0, 1, 2);

	this.btnC5 = new lib.btnC();
	this.btnC5.name = "btnC5";
	this.btnC5.setTransform(714.3,599.55);
	new cjs.ButtonHelper(this.btnC5, 0, 1, 2);

	this.btnB5 = new lib.btnB();
	this.btnB5.name = "btnB5";
	this.btnB5.setTransform(555.5,599.55);
	new cjs.ButtonHelper(this.btnB5, 0, 1, 2);

	this.btnA5 = new lib.btnA();
	this.btnA5.name = "btnA5";
	this.btnA5.setTransform(396.7,599.55);
	new cjs.ButtonHelper(this.btnA5, 0, 1, 2);

	this.btnD6 = new lib.btnD();
	this.btnD6.name = "btnD6";
	this.btnD6.setTransform(874.15,599.55);
	new cjs.ButtonHelper(this.btnD6, 0, 1, 2);

	this.btnC6 = new lib.btnC();
	this.btnC6.name = "btnC6";
	this.btnC6.setTransform(714.3,599.55);
	new cjs.ButtonHelper(this.btnC6, 0, 1, 2);

	this.btnB6 = new lib.btnB();
	this.btnB6.name = "btnB6";
	this.btnB6.setTransform(555.5,599.55);
	new cjs.ButtonHelper(this.btnB6, 0, 1, 2);

	this.btnA6 = new lib.btnA();
	this.btnA6.name = "btnA6";
	this.btnA6.setTransform(396.7,599.55);
	new cjs.ButtonHelper(this.btnA6, 0, 1, 2);

	this.btnD7 = new lib.btnD();
	this.btnD7.name = "btnD7";
	this.btnD7.setTransform(874.15,599.55);
	new cjs.ButtonHelper(this.btnD7, 0, 1, 2);

	this.btnC7 = new lib.btnC();
	this.btnC7.name = "btnC7";
	this.btnC7.setTransform(714.3,599.55);
	new cjs.ButtonHelper(this.btnC7, 0, 1, 2);

	this.btnB7 = new lib.btnB();
	this.btnB7.name = "btnB7";
	this.btnB7.setTransform(555.5,599.55);
	new cjs.ButtonHelper(this.btnB7, 0, 1, 2);

	this.btnA7 = new lib.btnA();
	this.btnA7.name = "btnA7";
	this.btnA7.setTransform(396.7,599.55);
	new cjs.ButtonHelper(this.btnA7, 0, 1, 2);

	this.btnD8 = new lib.btnD();
	this.btnD8.name = "btnD8";
	this.btnD8.setTransform(874.15,599.55);
	new cjs.ButtonHelper(this.btnD8, 0, 1, 2);

	this.btnC8 = new lib.btnC();
	this.btnC8.name = "btnC8";
	this.btnC8.setTransform(714.3,599.55);
	new cjs.ButtonHelper(this.btnC8, 0, 1, 2);

	this.btnB8 = new lib.btnB();
	this.btnB8.name = "btnB8";
	this.btnB8.setTransform(555.5,599.55);
	new cjs.ButtonHelper(this.btnB8, 0, 1, 2);

	this.btnA8 = new lib.btnA();
	this.btnA8.name = "btnA8";
	this.btnA8.setTransform(396.7,599.55);
	new cjs.ButtonHelper(this.btnA8, 0, 1, 2);

	this.btnD9 = new lib.btnD();
	this.btnD9.name = "btnD9";
	this.btnD9.setTransform(874.15,599.55);
	new cjs.ButtonHelper(this.btnD9, 0, 1, 2);

	this.btnC9 = new lib.btnC();
	this.btnC9.name = "btnC9";
	this.btnC9.setTransform(714.3,599.55);
	new cjs.ButtonHelper(this.btnC9, 0, 1, 2);

	this.btnB9 = new lib.btnB();
	this.btnB9.name = "btnB9";
	this.btnB9.setTransform(555.5,599.55);
	new cjs.ButtonHelper(this.btnB9, 0, 1, 2);

	this.btnA9 = new lib.btnA();
	this.btnA9.name = "btnA9";
	this.btnA9.setTransform(396.7,599.55);
	new cjs.ButtonHelper(this.btnA9, 0, 1, 2);

	this.btnD10 = new lib.btnD();
	this.btnD10.name = "btnD10";
	this.btnD10.setTransform(874.15,599.55);
	new cjs.ButtonHelper(this.btnD10, 0, 1, 2);

	this.btnC10 = new lib.btnC();
	this.btnC10.name = "btnC10";
	this.btnC10.setTransform(714.3,599.55);
	new cjs.ButtonHelper(this.btnC10, 0, 1, 2);

	this.btnB10 = new lib.btnB();
	this.btnB10.name = "btnB10";
	this.btnB10.setTransform(555.5,599.55);
	new cjs.ButtonHelper(this.btnB10, 0, 1, 2);

	this.btnA10 = new lib.btnA();
	this.btnA10.name = "btnA10";
	this.btnA10.setTransform(396.7,599.55);
	new cjs.ButtonHelper(this.btnA10, 0, 1, 2);

	this.btnNext13 = new lib.btnNext();
	this.btnNext13.name = "btnNext13";
	this.btnNext13.setTransform(1191.05,500.9);
	new cjs.ButtonHelper(this.btnNext13, 0, 1, 2);

	this.btnPrev14 = new lib.btnPrev();
	this.btnPrev14.name = "btnPrev14";
	this.btnPrev14.setTransform(1191.05,265.45);
	new cjs.ButtonHelper(this.btnPrev14, 0, 1, 2);

	this.btnLanjut1 = new lib.btnLanjut();
	this.btnLanjut1.name = "btnLanjut1";
	this.btnLanjut1.setTransform(640,578.65);
	new cjs.ButtonHelper(this.btnLanjut1, 0, 1, 2);

	this.btnLanjut2 = new lib.btnLanjut();
	this.btnLanjut2.name = "btnLanjut2";
	this.btnLanjut2.setTransform(640,578.65);
	new cjs.ButtonHelper(this.btnLanjut2, 0, 1, 2);

	this.btnLanjut3 = new lib.btnLanjut();
	this.btnLanjut3.name = "btnLanjut3";
	this.btnLanjut3.setTransform(640,578.65);
	new cjs.ButtonHelper(this.btnLanjut3, 0, 1, 2);

	this.btnLanjut4 = new lib.btnLanjut();
	this.btnLanjut4.name = "btnLanjut4";
	this.btnLanjut4.setTransform(640,578.65);
	new cjs.ButtonHelper(this.btnLanjut4, 0, 1, 2);

	this.btnLanjut5 = new lib.btnLanjut();
	this.btnLanjut5.name = "btnLanjut5";
	this.btnLanjut5.setTransform(640,578.65);
	new cjs.ButtonHelper(this.btnLanjut5, 0, 1, 2);

	this.btnLanjut6 = new lib.btnLanjut();
	this.btnLanjut6.name = "btnLanjut6";
	this.btnLanjut6.setTransform(640,578.65);
	new cjs.ButtonHelper(this.btnLanjut6, 0, 1, 2);

	this.btnLanjut7 = new lib.btnLanjut();
	this.btnLanjut7.name = "btnLanjut7";
	this.btnLanjut7.setTransform(640,578.65);
	new cjs.ButtonHelper(this.btnLanjut7, 0, 1, 2);

	this.btnLanjut8 = new lib.btnLanjut();
	this.btnLanjut8.name = "btnLanjut8";
	this.btnLanjut8.setTransform(640,578.65);
	new cjs.ButtonHelper(this.btnLanjut8, 0, 1, 2);

	this.btnLanjut9 = new lib.btnLanjut();
	this.btnLanjut9.name = "btnLanjut9";
	this.btnLanjut9.setTransform(640,578.65);
	new cjs.ButtonHelper(this.btnLanjut9, 0, 1, 2);

	this.btnLanjut10 = new lib.btnLanjut();
	this.btnLanjut10.name = "btnLanjut10";
	this.btnLanjut10.setTransform(640,578.65);
	new cjs.ButtonHelper(this.btnLanjut10, 0, 1, 2);

	this.btnCobalagi1 = new lib.btnCobalagi();
	this.btnCobalagi1.name = "btnCobalagi1";
	this.btnCobalagi1.setTransform(640,590.95);
	new cjs.ButtonHelper(this.btnCobalagi1, 0, 1, 2);

	this.btnCobalagi2 = new lib.btnCobalagi();
	this.btnCobalagi2.name = "btnCobalagi2";
	this.btnCobalagi2.setTransform(640,590.95);
	new cjs.ButtonHelper(this.btnCobalagi2, 0, 1, 2);

	this.btnCobalagi3 = new lib.btnCobalagi();
	this.btnCobalagi3.name = "btnCobalagi3";
	this.btnCobalagi3.setTransform(640,590.95);
	new cjs.ButtonHelper(this.btnCobalagi3, 0, 1, 2);

	this.btnCobalagi4 = new lib.btnCobalagi();
	this.btnCobalagi4.name = "btnCobalagi4";
	this.btnCobalagi4.setTransform(640,590.95);
	new cjs.ButtonHelper(this.btnCobalagi4, 0, 1, 2);

	this.btnCobalagi5 = new lib.btnCobalagi();
	this.btnCobalagi5.name = "btnCobalagi5";
	this.btnCobalagi5.setTransform(640,590.95);
	new cjs.ButtonHelper(this.btnCobalagi5, 0, 1, 2);

	this.btnCobalagi6 = new lib.btnCobalagi();
	this.btnCobalagi6.name = "btnCobalagi6";
	this.btnCobalagi6.setTransform(640,590.95);
	new cjs.ButtonHelper(this.btnCobalagi6, 0, 1, 2);

	this.btnCobalagi7 = new lib.btnCobalagi();
	this.btnCobalagi7.name = "btnCobalagi7";
	this.btnCobalagi7.setTransform(640,590.95);
	new cjs.ButtonHelper(this.btnCobalagi7, 0, 1, 2);

	this.btnCobalagi8 = new lib.btnCobalagi();
	this.btnCobalagi8.name = "btnCobalagi8";
	this.btnCobalagi8.setTransform(640,590.95);
	new cjs.ButtonHelper(this.btnCobalagi8, 0, 1, 2);

	this.btnCobalagi9 = new lib.btnCobalagi();
	this.btnCobalagi9.name = "btnCobalagi9";
	this.btnCobalagi9.setTransform(640,590.95);
	new cjs.ButtonHelper(this.btnCobalagi9, 0, 1, 2);

	this.btnCobalagi10 = new lib.btnCobalagi();
	this.btnCobalagi10.name = "btnCobalagi10";
	this.btnCobalagi10.setTransform(640,590.95);
	new cjs.ButtonHelper(this.btnCobalagi10, 0, 1, 2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.btnNext1}]},3).to({state:[{t:this.btnNext2},{t:this.btnPrev2}]},1).to({state:[{t:this.btnNext3},{t:this.btnPrev3}]},1).to({state:[{t:this.btnPrev4}]},1).to({state:[{t:this.btnNext5}]},1).to({state:[{t:this.btnNext6},{t:this.btnPrev6}]},1).to({state:[{t:this.btnNext7},{t:this.btnPrev7}]},1).to({state:[{t:this.btnPrev8}]},1).to({state:[{t:this.btnNext9}]},1).to({state:[{t:this.btnNext10},{t:this.btnPrev10}]},1).to({state:[{t:this.btnNext11},{t:this.btnPrev11}]},1).to({state:[{t:this.btnPrev12}]},1).to({state:[{t:this.btnA1},{t:this.btnB1},{t:this.btnC1},{t:this.btnD1}]},1).to({state:[{t:this.btnA2},{t:this.btnB2},{t:this.btnC2},{t:this.btnD2}]},1).to({state:[{t:this.btnA3},{t:this.btnB3},{t:this.btnC3},{t:this.btnD3}]},1).to({state:[{t:this.btnA4},{t:this.btnB4},{t:this.btnC4},{t:this.btnD4}]},1).to({state:[{t:this.btnA5},{t:this.btnB5},{t:this.btnC5},{t:this.btnD5}]},1).to({state:[{t:this.btnA6},{t:this.btnB6},{t:this.btnC6},{t:this.btnD6}]},1).to({state:[{t:this.btnA7},{t:this.btnB7},{t:this.btnC7},{t:this.btnD7}]},1).to({state:[{t:this.btnA8},{t:this.btnB8},{t:this.btnC8},{t:this.btnD8}]},1).to({state:[{t:this.btnA9},{t:this.btnB9},{t:this.btnC9},{t:this.btnD9}]},1).to({state:[{t:this.btnA10},{t:this.btnB10},{t:this.btnC10},{t:this.btnD10}]},1).to({state:[{t:this.btnNext13}]},1).to({state:[{t:this.btnPrev14}]},1).to({state:[{t:this.btnLanjut1}]},1).to({state:[{t:this.btnLanjut2}]},1).to({state:[{t:this.btnLanjut3}]},1).to({state:[{t:this.btnLanjut4}]},1).to({state:[{t:this.btnLanjut5}]},1).to({state:[{t:this.btnLanjut6}]},1).to({state:[{t:this.btnLanjut7}]},1).to({state:[{t:this.btnLanjut8}]},1).to({state:[{t:this.btnLanjut9}]},1).to({state:[{t:this.btnLanjut10}]},1).to({state:[{t:this.btnCobalagi1}]},1).to({state:[{t:this.btnCobalagi2}]},1).to({state:[{t:this.btnCobalagi3}]},1).to({state:[{t:this.btnCobalagi4}]},1).to({state:[{t:this.btnCobalagi5}]},1).to({state:[{t:this.btnCobalagi6}]},1).to({state:[{t:this.btnCobalagi7}]},1).to({state:[{t:this.btnCobalagi8}]},1).to({state:[{t:this.btnCobalagi9}]},1).to({state:[{t:this.btnCobalagi10}]},1).to({state:[]},1).wait(1));

	// Graphic
	this.mcGuide = new lib.mcGuide();
	this.mcGuide.name = "mcGuide";
	this.mcGuide.setTransform(644.95,375.75);

	this.mcIntro = new lib.mcIntro();
	this.mcIntro.name = "mcIntro";
	this.mcIntro.setTransform(855.2,465);

	this.instance = new lib.mcHome();
	this.instance.setTransform(640,360);

	this.instance_1 = new lib.grTense1("synched",0);
	this.instance_1.setTransform(640,360);

	this.instance_2 = new lib.grTense2("synched",0);
	this.instance_2.setTransform(640,360);

	this.instance_3 = new lib.grTense3("synched",0);
	this.instance_3.setTransform(640,360);

	this.instance_4 = new lib.grTense4("synched",0);
	this.instance_4.setTransform(640,360);

	this.instance_5 = new lib.grTense5("synched",0);
	this.instance_5.setTransform(640,360);

	this.instance_6 = new lib.grTense6("synched",0);
	this.instance_6.setTransform(640,360);

	this.instance_7 = new lib.grTense7("synched",0);
	this.instance_7.setTransform(640,360);

	this.instance_8 = new lib.grTense8("synched",0);
	this.instance_8.setTransform(640,360);

	this.instance_9 = new lib.grTense9("synched",0);
	this.instance_9.setTransform(640,360);

	this.instance_10 = new lib.grTense10("synched",0);
	this.instance_10.setTransform(640,360);

	this.instance_11 = new lib.grTense11("synched",0);
	this.instance_11.setTransform(640,360);

	this.instance_12 = new lib.grTense12("synched",0);
	this.instance_12.setTransform(640,360);

	this.instance_13 = new lib.CachedBmp_2315();
	this.instance_13.setTransform(141.6,120.85,0.5,0.5);

	this.instance_14 = new lib.CachedBmp_2316();
	this.instance_14.setTransform(140.2,118.8,0.5,0.5);

	this.instance_15 = new lib.CachedBmp_2317();
	this.instance_15.setTransform(141.6,120.85,0.5,0.5);

	this.instance_16 = new lib.CachedBmp_2318();
	this.instance_16.setTransform(141.6,120.85,0.5,0.5);

	this.instance_17 = new lib.CachedBmp_2319();
	this.instance_17.setTransform(141.6,120.85,0.5,0.5);

	this.instance_18 = new lib.CachedBmp_2320();
	this.instance_18.setTransform(141.6,120.85,0.5,0.5);

	this.instance_19 = new lib.CachedBmp_2321();
	this.instance_19.setTransform(141.6,120.85,0.5,0.5);

	this.instance_20 = new lib.CachedBmp_2322();
	this.instance_20.setTransform(141.6,120.85,0.5,0.5);

	this.instance_21 = new lib.CachedBmp_2323();
	this.instance_21.setTransform(141.6,120.85,0.5,0.5);

	this.instance_22 = new lib.CachedBmp_2324();
	this.instance_22.setTransform(141.6,120.85,0.5,0.5);

	this.instance_23 = new lib.grGloss1("synched",0);
	this.instance_23.setTransform(640,360);

	this.instance_24 = new lib.grGloss2("synched",0);
	this.instance_24.setTransform(640,360);

	this.instance_25 = new lib.CachedBmp_2325();
	this.instance_25.setTransform(0,0,0.5,0.5);

	this.instance_26 = new lib.CachedBmp_2326();
	this.instance_26.setTransform(0,0,0.5,0.5);

	this.btnCong = new lib.btnCong();
	this.btnCong.name = "btnCong";
	this.btnCong.setTransform(640,596.5);
	new cjs.ButtonHelper(this.btnCong, 0, 1, 2);

	this.instance_27 = new lib.CachedBmp_2327();
	this.instance_27.setTransform(0,0,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.mcGuide}]}).to({state:[{t:this.mcIntro}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_11}]},1).to({state:[{t:this.instance_12}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_16}]},1).to({state:[{t:this.instance_17}]},1).to({state:[{t:this.instance_18}]},1).to({state:[{t:this.instance_19}]},1).to({state:[{t:this.instance_20}]},1).to({state:[{t:this.instance_21}]},1).to({state:[{t:this.instance_22}]},1).to({state:[{t:this.instance_23}]},1).to({state:[{t:this.instance_24}]},1).to({state:[{t:this.instance_25}]},1).to({state:[{t:this.instance_26}]},10).to({state:[{t:this.instance_27},{t:this.btnCong}]},10).wait(1));

	// Background
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#2C2C3A").s().p("Ehj/A4QMAAAhwfMDH/AAAMAAABwfg");
	this.shape.setTransform(640,360);

	this.instance_28 = new lib.CachedBmp_2328();
	this.instance_28.setTransform(0,0,0.5,0.5);

	this.instance_29 = new lib.CachedBmp_2329();
	this.instance_29.setTransform(0,0,0.5,0.5);

	this.instance_30 = new lib.CachedBmp_2330();
	this.instance_30.setTransform(0,0,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.instance_28}]},1).to({state:[{t:this.instance_29}]},14).to({state:[{t:this.instance_30}]},10).to({state:[]},5).wait(18));

	// stageBackground
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,0,0,0)").ss(1,1,1,3,true).p("Ehljg5zMDLHAAAMAAABznMjLHAAAg");
	this.shape_1.setTransform(640,360);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("EhljA50MAAAhznMDLHAAAMAAABzng");
	this.shape_2.setTransform(640,360);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1}]}).wait(48));

	this._renderFirstFrame();

}).prototype = p = new lib.AnMovieClip();
p.nominalBounds = new cjs.Rectangle(-490.9,349,2707.1,382);
// library properties:
lib.properties = {
	id: 'DB47348BF12876418E4E33F6B8343B34',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/CachedBmp_2162.png", id:"CachedBmp_2162"},
		{src:"images/CachedBmp_2161.png", id:"CachedBmp_2161"},
		{src:"images/CachedBmp_2160.png", id:"CachedBmp_2160"},
		{src:"images/CachedBmp_2159.png", id:"CachedBmp_2159"},
		{src:"images/CachedBmp_2158.png", id:"CachedBmp_2158"},
		{src:"images/CachedBmp_2157.png", id:"CachedBmp_2157"},
		{src:"images/CachedBmp_2156.png", id:"CachedBmp_2156"},
		{src:"images/CachedBmp_2155.png", id:"CachedBmp_2155"},
		{src:"images/CachedBmp_2154.png", id:"CachedBmp_2154"},
		{src:"images/CachedBmp_2153.png", id:"CachedBmp_2153"},
		{src:"images/CachedBmp_2152.png", id:"CachedBmp_2152"},
		{src:"images/CachedBmp_2151.png", id:"CachedBmp_2151"},
		{src:"images/CachedBmp_2349.png", id:"CachedBmp_2349"},
		{src:"images/CachedBmp_2146.png", id:"CachedBmp_2146"},
		{src:"images/CachedBmp_2144.png", id:"CachedBmp_2144"},
		{src:"images/CachedBmp_2143.png", id:"CachedBmp_2143"},
		{src:"images/CachedBmp_2330.png", id:"CachedBmp_2330"},
		{src:"images/CachedBmp_2329.png", id:"CachedBmp_2329"},
		{src:"images/CachedBmp_2328.png", id:"CachedBmp_2328"},
		{src:"images/CachedBmp_2327.png", id:"CachedBmp_2327"},
		{src:"images/CachedBmp_2326.png", id:"CachedBmp_2326"},
		{src:"images/CachedBmp_2325.png", id:"CachedBmp_2325"},
		{src:"images/CachedBmp_2324.png", id:"CachedBmp_2324"},
		{src:"images/CachedBmp_2323.png", id:"CachedBmp_2323"},
		{src:"images/CachedBmp_2322.png", id:"CachedBmp_2322"},
		{src:"images/CachedBmp_2321.png", id:"CachedBmp_2321"},
		{src:"images/CachedBmp_2320.png", id:"CachedBmp_2320"},
		{src:"images/CachedBmp_2319.png", id:"CachedBmp_2319"},
		{src:"images/CachedBmp_2318.png", id:"CachedBmp_2318"},
		{src:"images/CachedBmp_2317.png", id:"CachedBmp_2317"},
		{src:"images/CachedBmp_2316.png", id:"CachedBmp_2316"},
		{src:"images/CachedBmp_2315.png", id:"CachedBmp_2315"},
		{src:"images/It_s Time For Tenses_atlas_1.png", id:"It_s Time For Tenses_atlas_1"},
		{src:"sounds/click.mp3", id:"click"},
		{src:"sounds/cong.mp3", id:"cong"},
		{src:"sounds/correct.mp3", id:"correct"},
		{src:"sounds/intro.mp3", id:"intro"},
		{src:"sounds/bgm.mp3", id:"bgm"},
		{src:"sounds/wrong.mp3", id:"wrong"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['DB47348BF12876418E4E33F6B8343B34'] = {
	getStage: function() { return exportRoot.stage; },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


an.makeResponsive = function(isResp, respDim, isScale, scaleType, domContainers) {		
	var lastW, lastH, lastS=1;		
	window.addEventListener('resize', resizeCanvas);		
	resizeCanvas();		
	function resizeCanvas() {			
		var w = lib.properties.width, h = lib.properties.height;			
		var iw = window.innerWidth, ih=window.innerHeight;			
		var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
		if(isResp) {                
			if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
				sRatio = lastS;                
			}				
			else if(!isScale) {					
				if(iw<w || ih<h)						
					sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==1) {					
				sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==2) {					
				sRatio = Math.max(xRatio, yRatio);				
			}			
		}
		domContainers[0].width = w * pRatio * sRatio;			
		domContainers[0].height = h * pRatio * sRatio;
		domContainers.forEach(function(container) {				
			container.style.width = w * sRatio + 'px';				
			container.style.height = h * sRatio + 'px';			
		});
		stage.scaleX = pRatio*sRatio;			
		stage.scaleY = pRatio*sRatio;
		lastW = iw; lastH = ih; lastS = sRatio;            
		stage.tickOnUpdate = false;            
		stage.update();            
		stage.tickOnUpdate = true;		
	}
}
an.handleSoundStreamOnTick = function(event) {
	if(!event.paused){
		var stageChild = stage.getChildAt(0);
		if(!stageChild.paused){
			stageChild.syncStreamSounds();
		}
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;